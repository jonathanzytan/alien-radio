$('#playPodBtn').on('click',playPodTrack);
$('#pausePodBtn').on('click',pausePodTrack);
$('#stopPodBtn').on('click',stopPodTrack);
$('#podcastBtn').on('click',playPodTrack);
$('#playRadBtn').on('click',playRadTrack);
$('#pauseRadBtn').on('click',pauseRadTrack);
$('#stopRadBtn').on('click',stopRadTrack);
$('#radioBtn').on('click',playRadTrack);
$('#volUpBtn').on('click',volumeUp);
$('#volDnBtn').on('click',volumeDown);


function playPodTrack(){
	$('#radio')[0].pause();
	$('#radio')[0].currentTime = 0;
	$('#podcast')[0].play();
}
function pausePodTrack(){
	$('#podcast')[0].pause();
}
function stopPodTrack(){
	$('#podcast')[0].pause();
	$('#podcast')[0].currentTime = 0;
}
function playRadTrack(){
	$('#podcast')[0].pause();
	$('#podcast')[0].currentTime = 0;
	$('#radio')[0].play();
}
function pauseRadTrack(){
	$('#radio')[0].pause();
}
function stopRadTrack(){
	$('#radio')[0].pause();
	$('#radio')[0].currentTime = 0;
}
function volumeUp(){
	$('#podcast')[0].volume = $('#podcast')[0].volume + 0.1;
	$('#radio')[0].volume = $('#radio')[0].volume + 0.1;
}
function volumeDown(){
	$('#podcast')[0].volume = $('#podcast')[0].volume - 0.1;
	$('#radio')[0].volume = $('#radio')[0].volume - 0.1;
}